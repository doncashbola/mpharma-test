# MPharma Exercise

## Requirements
* terraform >= 0.15
* ansible >= 2.9
* Ansible docker plugin `ansible-galaxy collection install community.docker`
* AWS account (access key and secret key)

## Assumptions
* The domain for reaching the app is predefined and a corresponding certificate is already added to AWS Certificate manager. In this exercise, the cert name is `mpharma_test` and the certificate domain is `mpharma-test.nehosystems.com`
* The key pair to be used in the ec2 host is also preinstalled in AWS. In the exercise, the key pair name is `mpharma`
* The terraform scripts make use of workspaces and the assumed one here is `test` hence in `terraform/locals.tf`, the default object contains variables with prefix `test`. If you wish to use another workspace, please add variables in the same format.


## Deploy
If all the above requirements are met, 
* run `deploy.sh` and follow the prompt

