locals {
    cidr	= "${lookup(var.environments, "${terraform.workspace}.cidr")}"
    region	= "${lookup(var.environments, "${terraform.workspace}.region")}"
     key_pair	= "${lookup(var.environments, "${terraform.workspace}.key_pair")}"

     alb_subnets = "${split(",", lookup(var.environments, "${terraform.workspace}.alb_subnets"))}"

    web_subnet = "${lookup(var.environments, "${terraform.workspace}.web_subnet")}"
}

variable "environments" {
    type = map(string)

    default = {
        "test.region" = "eu-west-1"
        "test.cidr" = "10.10.0.0/16"
        "test.web_subnet" = "10.10.1.0/24"
        "test.alb_subnets"		= "10.10.4.0/24,10.10.5.0/24"	
        "test.key_pair" = "mpharma"
    }
}