variable "env" {}

variable "web_alb_security_group" {}

variable "web_subnet_ids" {}

variable "alb_subnet_ids" {
	type		= list
}

variable "vpc_id" {}

variable "web_ec2_id" {}

#variable "proxy_cert_arn" {}