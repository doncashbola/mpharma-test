data "aws_acm_certificate" "mpharma_test" {
	domain		= "mpharma-test.nehosystems.com"
	statuses	= ["ISSUED"]
	most_recent = true
}

resource "aws_alb" "web_alb" {
  name		= "mpharma-web-${var.env}"
  internal	= "false"
  security_groups	= ["${var.web_alb_security_group}"]
  subnets			= "${var.alb_subnet_ids}"

  tags = {
	  Name = "onvp-api-${var.env}"
  }
}


resource "aws_alb_target_group" "web_alb" {
  name     = "mpharma-web-${var.env}"
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${var.vpc_id}"
  target_type = "instance"

  deregistration_delay = "300"

  health_check {
    protocol            = "HTTP"
    path                = "/"
    port                = "traffic-port"
    healthy_threshold   = 5
    unhealthy_threshold = 3
    timeout             = 5
    interval            = 30
    matcher             = "200"
  }
}


resource "aws_lb_target_group_attachment" "web" {
  target_group_arn = "${aws_alb_target_group.web_alb.arn}"
  target_id        = "${var.web_ec2_id}"
  port             = 80
}

resource "aws_alb_listener" "web_alb_http" {
  load_balancer_arn = "${aws_alb.web_alb.arn}"
  port              = 80

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_alb_listener" "web_alb_https" {
  load_balancer_arn = "${aws_alb.web_alb.arn}"
  port              = 443
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = "${data.aws_acm_certificate.mpharma_test.arn}"

  default_action {
    target_group_arn = "${aws_alb_target_group.web_alb.arn}"
    type             = "forward"
  }
}
