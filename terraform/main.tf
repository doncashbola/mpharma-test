###########################
# TERRAFORM CONFIG
###########################
provider "aws" {
    region  = "eu-west-1"   
}

data "aws_availability_zones" "available" {}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.41.0"
    }
  }
    backend "s3" {
        encrypt                 = true
        bucket                  = "mpharma-test"
        dynamodb_table          = "mpharma-terraform-locks"
        region                  = "eu-west-1"
        key                     = "mpharma-test-terraform"
        workspace_key_prefix    = "workspace"
    }
}


module "vpc" {
  source = "./vpc"
  env							= "${terraform.workspace}"
  azs					= "${data.aws_availability_zones.available.names}"
  cidr					= "${local.cidr}"
  region				= "${local.region}"
  web_subnet			= "${local.web_subnet}"
  alb_subnets			= "${local.alb_subnets}"
 
}


module "sg" {
  source = "./security_groups"
  env							= "${terraform.workspace}"
  vpc_id						= "${module.vpc.id}"
}



module "ec2" {
    source = "./ec2"
    web_security_groups  = ["${module.sg.web_id}"]
    web_subnet						= 	"${module.vpc.web_subnets_id}"
    key_pair =        "${local.key_pair}"
}


module "lb" {
  source = "./load_balancer"
  env		= "${terraform.workspace}"
  vpc_id	= "${module.vpc.id}"
  #proxy_cert_arn			= "${module.acm.proxy_arn}"
  web_alb_security_group	= "${module.sg.alb_id}"
  web_subnet_ids		= "${module.vpc.web_subnets_id}"
  alb_subnet_ids		= "${module.vpc.alb_subnet_ids}"
  web_ec2_id = "${module.ec2.web_id}"
  }


output "web_ip" {
  value = "${module.ec2.web_ip}"
}

output "alb_url" {
  value = "${module.lb.alb_url}"
}
