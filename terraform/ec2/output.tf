output "web_ip" {
	value	= "${aws_instance.web.public_ip}"
}

output "web_id" {
    value = "${aws_instance.web.id}"
}