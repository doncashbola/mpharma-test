variable "web_security_groups" {
    type = list
}

variable "web_subnet" {}

variable "key_pair" {}