data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] 
}


resource "aws_instance" "web" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"

  security_groups             = "${var.web_security_groups}"
  subnet_id                 = "${var.web_subnet}"
  associate_public_ip_address = "true"
  key_name                    = "${var.key_pair}"

  tags = {
    Name = "mpharma-test"
  }
}