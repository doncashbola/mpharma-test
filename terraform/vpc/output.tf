
output "web_subnets_id" {
  value       = "${aws_subnet.web.id}"
}

output "alb_subnet_ids" {
  value       = "${aws_subnet.alb.*.id}"
}

output "id" {
  value = "${aws_vpc.this.id}"
}

