######
# VPC
######
resource "aws_vpc" "this" {
	cidr_block				= var.cidr
	enable_dns_hostnames	= "true"
	enable_dns_support		= "true"

	tags = {
		Name = "mpharma-${var.env}"
	}
}


##################
# Internet Gateway
##################
resource "aws_internet_gateway" "this" {
	vpc_id 	= aws_vpc.this.id
	tags = {
		Name = "mpharma-${var.env}"
	}
}



###################
# WEB Subnet 
###################
resource "aws_subnet" "web" {
	vpc_id				= aws_vpc.this.id
	cidr_block			= var.web_subnet
	map_public_ip_on_launch		= true
	availability_zone = "${element(var.azs, 0)}"
	tags = {
		Name = "web-mpharma-${var.env}}"
	}
}


###################
# ALB Subnet 
###################
resource "aws_subnet" "alb" {
	count				= "2"
	vpc_id				= aws_vpc.this.id
	cidr_block			= var.alb_subnets[count.index]
	availability_zone	= element(var.azs, count.index)
	map_public_ip_on_launch		= true
	tags = {
		Name = "alb-mpharma-${var.env}-${element(var.azs, count.index)}"
	}
}




###################
# Route Table
###################
resource "aws_route_table" "mpharma" {
  vpc_id = "${aws_vpc.this.id}"

  tags = {
	Name = "mpharma-${var.env}-${var.region}"
	Environment	= "${var.env}"
  }
}



############################
# VPC Main Route Table Assoc
############################

resource "aws_main_route_table_association" "this" {
	vpc_id				= aws_vpc.this.id
	route_table_id		= aws_route_table.mpharma.id
}


##################
# Default Route(s)
##################
resource "aws_route" "mpharma_default" {
	route_table_id			= aws_route_table.mpharma.id
	destination_cidr_block 	= "0.0.0.0/0"
	gateway_id				= aws_internet_gateway.this.id
}


###################
# Route Table Assoc
###################
resource "aws_route_table_association" "web" {
	subnet_id 		=	aws_subnet.web.id
	route_table_id	= 	aws_route_table.mpharma.id
}

