variable "env" {}

variable "region" {}

variable "cidr" {}

variable "web_subnet" {}

variable "alb_subnets" {
	type			= list
}


variable "azs" {
	description		= "A list of availability zones in the region"
	type			= list
	default			= []
}
