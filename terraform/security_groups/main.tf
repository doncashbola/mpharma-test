
resource "aws_security_group" "web" {
    name        = "mpharma-web-${var.env}"
    vpc_id      = "${var.vpc_id}"

    tags = {
        Name = "mpharma-${var.env}"
    }
}

resource "aws_security_group" "alb" {
    name        = "mpharma-alb-${var.env}"
    vpc_id      = "${var.vpc_id}"

    tags = {
        Name = "mpharma-${var.env}"
    }
}

resource "aws_security_group_rule" "allow-all-web-alb" {
	type				= "ingress"
	security_group_id	= "${aws_security_group.web.id}"
	source_security_group_id	= "${aws_security_group.alb.id}"
	description		= "Allow all traffic from  alb"

	from_port	= 0
	to_port		= 65535
	protocol	= "-1"
}

resource "aws_security_group_rule" "web-allow-all-ssh" {
    type                = "ingress"
    security_group_id   = "${aws_security_group.web.id}"
    cidr_blocks         = ["0.0.0.0/0"]

    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
}

resource "aws_security_group_rule" "web-allow-egress" {
    type                = "egress"
    security_group_id   = "${aws_security_group.web.id}"
    cidr_blocks         = ["0.0.0.0/0"]

    from_port   = 0
    to_port     = 65535
    protocol    = "-1"
}



resource "aws_security_group_rule" "alb-allow-all-http" {
    type                = "ingress"
    security_group_id   = "${aws_security_group.alb.id}"
    cidr_blocks         = ["0.0.0.0/0"]

    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
}

resource "aws_security_group_rule" "alb-allow-all-https" {
    type                = "ingress"
    security_group_id   = "${aws_security_group.alb.id}"
    cidr_blocks         = ["0.0.0.0/0"]

    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
}



resource "aws_security_group_rule" "alb-allow-egress" {
    type                = "egress"
    security_group_id   = "${aws_security_group.alb.id}"
    cidr_blocks         = ["0.0.0.0/0"]

    from_port   = 0
    to_port     = 65535
    protocol    = "-1"
}
