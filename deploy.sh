#!/bin/bash

if [[ -z "$AWS_SECRET_ACCESS_KEY" ]] || [[ -z "$AWS_SECRET_ACCESS_KEY" ]]; then
  echo "AWS env variables not set!!!"
  exit 56
fi

cd terraform

terraform init && terraform apply

if [ $? != 0 ]; then 
    exit 56
fi

WEB_IP=`terraform output -raw web_ip`
ALB_URL=`terraform output -raw alb_url`


cd ..
echo "web ansible_host=$WEB_IP" > ansible/hosts

cd ansible
ansible-playbook web-playbook.yml -i hosts


echo "############################################################################################################################################"
echo "create a CNAME DNS record for mpharma-test.nehosystems.com -> $ALB_URL"
echo "############################################################################################################################################"

